#! /bin/bash
#
# check_keytab.bash
# Copyright (C) 2019 Bruno Durand <bruno.z.durand@gmail.com>
#
# Distributed under terms of the MIT license.
# 
# We use this script for testing our keytabs

if [[ $# -ne 2 ]]
then
  echo "Usage: $0 keytab user"
  exit 1
fi

echo "------------------"

# Checking UPN format
# 
function checkupn {
  upn=$1
  if [ $(echo $upn | grep "HTTPS/" | wc -l) -ne 0 ]
  then
    echo "${upn} is not valid. It must NOT contain HTTPS/, please set it as HTTP/<DNS>@<KDC-REALM> instead. KO"
    echo "Exiting..."
    exit 1
  fi
  if [ $(echo $upn | grep ":" | wc -l) -ne 0 ]
  then
    echo "${upn} is not valid. It must NOT contain :, please set it as HTTP/<DNS>@<KDC-REALM> instead. KO"
    echo "Exiting..."
    exit 1
  fi
  if [ $(echo $upn | sed -n '/^HTTP\/.\+@.\+$/p' | wc -l) -eq 0 ]
  then
    echo "${upn} is not valid. It must be set as HTTP/<DNS>@<KDC-REALM>. KO"
    echo "Exiting..."
    exit 1
  fi
  return 0
}
 

keytab=$1
ldapuser=$2
 
kvnol=$(klist -k -e $keytab | tail -n +4 | awk '{ print $1 }')
upnl=$(klist -k -e $keytab | tail -n +4 | awk '{ print $2 }')
cipherl=$(klist -k -e $keytab | tail -n +4 | awk '{ print $3 }' | tr -d '()')
 
# Le format de sortie de klist oblige a passer par des tableaux pour le parsing
kvnos=()
upns=()
ciphers=()
for i in $kvnol
do
  kvnos+=($i)
done
for i in $upnl
do
  upns+=($i)
done
for i in $cipherl
do
  ciphers+=($i)
done
 
nblines=`expr ${#kvnos[@]} - 1`
 
# Le mot de passe peut etre different en fonction du domaine ; dans le cas ou plusieurs domaines sont references dans le keytab avec des mots de passe differents on passe par une map. La limitation est que le compte doit etre identique sur tous les domaines concernes.
declare -A passwords
 
# Comme un keytab peut contenir plusieurs fois le meme SPN avec chacun un cipher different, cette map permet de verifier si au moins AES128 et AES256 sont renseignes dans le keytab et actifs sur le compte. On part du principe qu'on ignore les autres algo.
declare -A accountAES128
declare -A accountAES256
declare -A accountCiphers
 
for i in `seq 0 ${nblines}`
do
  kvno=${kvnos[$i]}
  upn=${upns[$i]}
  cipher=${ciphers[$i]}
  spn=$(echo $upn | cut -d'@' -f1)
  domain=$(echo $upn | cut -d'@' -f2)
  uri=$(echo $spn | cut -d'/' -f2)
 
  checkupn "$upn"
 
  if [ -z "${passwords[$domain]}" ]
  then
    echo -n "${ldapuser} in ${domain} password: "
    read -s pass
    passwords+=([$domain]=$pass)
    echo
  fi
 
  sAMAccountName=$(ldapsearch -x -h ${domain} -D "${ldapuser}@${domain}" -w "${passwords[$domain]}" -b "DC=$(echo $domain | cut -d'.' -f1),DC=$(echo $domain | cut -d'.' -f2),DC=$(echo $domain | cut -d'.' -f3)" "(&(objectClass=user)(servicePrincipalName=${spn}))" sAMAccountName | grep sAMAccountName | cut -d' ' -f2 | grep -v "requesting:")
 
  userAccountControl=$(ldapsearch -x -h ${domain} -D "${ldapuser}@${domain}" -w "${passwords[$domain]}" -b "DC=$(echo $domain | cut -d'.' -f1),DC=$(echo $domain | cut -d'.' -f2),DC=$(echo $domain | cut -d'.' -f3)" "(&(objectClass=user)(sAMAccountName=${sAMAccountName}))" userAccountControl | grep userAccountControl | cut -d' ' -f2 | grep -v "requesting:")
 
  supportedEncryptionTypes=$(ldapsearch -x -h ${domain} -D "${ldapuser}@${domain}" -w "${passwords[$domain]}" -b "DC=$(echo $domain | cut -d'.' -f1),DC=$(echo $domain | cut -d'.' -f2),DC=$(echo $domain | cut -d'.' -f3)" "(&(objectClass=user)(sAMAccountName=${sAMAccountName}))" msDS-SupportedEncryptionTypes | grep "msDS-SupportedEncryptionTypes" | cut -d' ' -f2 | grep -v "requesting:")
 
  keyVersionNumber=$(ldapsearch -x -h ${domain} -D "${ldapuser}@${domain}" -w "${passwords[$domain]}" -b "DC=$(echo $domain | cut -d'.' -f1),DC=$(echo $domain | cut -d'.' -f2),DC=$(echo $domain | cut -d'.' -f3)" "(&(objectClass=user)(sAMAccountName=${sAMAccountName}))" msDS-KeyVersionNumber | grep "msDS-KeyVersionNumber" | cut -d' ' -f2 | grep -v "requesting:")
 
  accountSpn=$(ldapsearch -x -h ${domain} -D "${ldapuser}@${domain}" -w ${passwords[$domain]} -b "DC=$(echo $domain | cut -d'.' -f1),DC=$(echo $domain | cut -d'.' -f2),DC=$(echo $domain | cut -d'.' -f3)" "(&(objectClass=user)(sAMAccountName=${sAMAccountName}))" servicePrincipalName | grep servicePrincipalName | cut -d' ' -f2 | grep -v "requesting:")
 
  if [ "x${sAMAccountName}" == "x" ]
  then
    echo "Could not retrieve account name linked to ${upn}!!! KO"
    echo "Exiting..."
    exit 1
  fi
  if [ $(echo "${sAMAccountName}" | wc -l) -ne 1 ]
  then
    echo "Multiple accounts are linked to ${upn}: KO"
    echo "${sAMAccountName}"
    echo "Exiting..."
    exit 1
  fi
  accountCiphers+=(["$sAMAccountName@$domain"]=$cipher)
 
  DES=""
  DESFlag=$((${userAccountControl} & 2097152))
  if [[ $DESFlag -eq 0 ]]
  then
    DES=false
  else
   DES=true
  fi
 
  if [ "x${supportedEncryptionTypes}" == "x" ]
  then
    supportedEncryptionTypes=0
  fi
 
  if [ -z "${accountAES256[$sAMAccountName@$domain]}" ]
  then
    AES256=""
    AES256Flag=$((${supportedEncryptionTypes} & 16))
    if [[ $AES256Flag -eq 0 ]]
    then
      AES256=false
    else
      AES256=true
    fi
    accountAES256+=(["$sAMAccountName@$domain"]=$AES256)
  fi
 
  if [ -z "${accountAES128[$sAMAccountName@$domain]}" ]
  then
    AES128=""
    AES128Flag=$((${supportedEncryptionTypes} & 8))
    if [[ $AES128Flag -eq 0 ]]
    then
      AES128=false
    else
      AES128=true
    fi
    accountAES128+=(["$sAMAccountName@$domain"]=$AES128)
  fi
 
  accountDisabled=""
  accountDisabledFlag=$((${userAccountControl} & 2))
  if [[ $accountDisabledFlag -eq 0 ]]
  then
    accountDisabled=false
  else
    accountDisabled=true
  fi
  accountNotExpires=""
  accountNotExpiresFlag=$((${userAccountControl} & 65536))
  if [[ $accountNotExpiresFlag -eq 0 ]]
  then
    accountNotExpires=false
  else
    accountNotExpires=true
  fi
 
  a=$(dig ${uri} +noquestion +noauthority +noadditional +nostats | grep ${uri} | tail -1 | awk '{print $4}')
 
  # Partie verifications
 
  if [ "${kvno}" == "${keyVersionNumber}" ]
  then
    echo "KVNO ${kvno} OK"
  else
    echo "KVNO MISMATCH!!! Please regenerate the keytab. KO"
    echo "Keytab: ${kvno} ${sAMAccountName}: ${keyVersionNumber}"
    echo "Exiting..."
    exit 1
  fi
 
  if [ "x${accountDisabled}" == "xtrue" ]
  then
    echo "${sAMAccountName} is disabled!!! KO"
    echo "Exiting..."
    exit 1
  fi
 
  if [ "x${accountNotExpires}" == "xfalse" ]
  then
    echo "${sAMAccountName} can expire. Please set the notExpires flag 0x10000 to this account. KO"
    echo "Exiting..."
    exit 1
  fi
 
  if [ "x${DES}" == "xtrue" ]
  then
    echo "DES is set for ${sAMAccountName}. DES must be disabled. KO"
    echo "Exiting..."
    exit 1
  fi
 
  if [ "x${a}" == "xCNAME" ]
  then
    echo "${spn} must NOT be a CNAME, it must be A. KO"
    echo "Exiting..."
    exit 1
  elif [ "x${a}" != "xA" ]
  then
    echo "WARN ${spn} could not be checked if it's type A or CNAME. Once the URI is registered please run this script again."
  else
    echo "${spn} is type ${a}. OK"
  fi
 
  if [ $(echo "${accountSpn}" | wc -l) -gt 1 ]
  then
    echo "WARN ${sAMAccountName} is linked to multiple SPN. This is NOT a best practice and may be the root cause if kinit fail!"
    echo "The SPN related to ${sAMAccountName} are:"
    echo "${accountSpn}"
  fi
  echo "------------------"
done
 
# Verifie si chaque compte a AES128 et/ou AES256 actif et le reste...
aesok=false
for account in ${!accountAES128[@]}
do
  if [ "x${accountAES128[$account]}" == "xtrue" ]
  then
    echo "$account has AES128 enabled. OK"
    if [ $(echo "${accountCiphers[$account]}" | grep "aes128" | wc -l) -eq 1 ]
    then
      echo "Keytab has AES128 cipher for SPN related to $account. OK"
      aesok=true
    fi
  fi
  if [ "x${accountAES256[$account]}" == "xtrue" ]
  then
    echo "$account has AES256 enabled. OK"
    if [ $(echo "${accountCiphers[$account]}" | grep "aes256" | wc -l) -eq 1 ]
    then
      echo "Keytab has AES256 cipher for SPN related to $account. OK"
      aesok=true
    fi
  else
    echo "WARN $account has AES256 disabled. Please set it to enable if possible."
  fi
  if [ "x${accountAES128[$account]}" == "xfalse" ] && [ "x${accountAES256[$account]}" == "xfalse" ]
  then
    echo "WARN Neither AES128 nor AES256 is set for ${account}. Please set AES256 if possible, AES128 otherwise."
  fi
  if [ "x${aesok}" == "xfalse" ]
  then
    echo "WARN Neither AES128 nor AES256 is used in a cipher in the Keytab. Please add AES (256 if possible) as a valid cipher, for example aes256-cts-hmac-sha1-96"
  fi
  # Les tests sur DES et actifs/inactifs sont passes au-dessus. Pour eviter de pourrir la sortie avec plusieurs fois OK on l'affiche ici
  echo "$account seems GOOD: it is not disabled, never expires and DES is disabled. OK"
done
 
echo "------------------"
 
# Tout passe, donc il reste le test du kinit
 
# Impossible d'enlever les doublons de upns ou upnl simplement. Si quelqu'un trouve un moyen d'enlever cette map...
declare -A upnChecked
 
for upn in ${upnl}
do
  if [ -z ${upnChecked[$upn]} ]
  then
    #echo "${upn}"
    upnChecked+=([$upn]=true)
    /usr/bin/kinit -v -k  $upn -t $keytab
    returncode=$?
    if [ $returncode -eq 0 ]
    then
      echo "kinit successful. Everything is OK."
    else
      echo "kinit failed!!! Please see message above! KO"
      echo "For full debug log, please run command below:"
      echo "KRB5_TRACE=/dev/stdout /usr/bin/kinit -v -k ${upn} -t ${keytab}"
    fi
    echo "------------------"
  fi
done
 
exit 0
