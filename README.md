# Keytab Validator

Little bash script to help checking keytabs with troubleshooting Kerberos

## Introduction

This script is a try to help with commons errors found in Keytab files and Active Directory configuration. You can find a kerberos troubleshooting with the documentation [here](https://gitlab.com/bruno.z.durand/keytab-validator/wikis/home).

## Prerequisites

In order to use this script you need ldapsearch, klist and dig installed on your platform.
On Centos/RHEL you can run `sudo yum install krb5-workstation openldap-clients bind-utils -y`

You also need an active account on the KDC Realm AD domain. If multiple KDC realms, You'll need as many account as you have differents KDC realms.

## Usage

`./check_keytab.bash keytab_file AD_user`

this isn't the user attached to the SPNs, it's the one used to read the AD.
| <span style="color:green">INFO:</span> If there are more than one KDC Realm, we'll try to log with the same user on the differents domains. |
| --- |

## Operations Descriptions

For each SPN we check :

1. The SPN format. It should be (HTTP/\<DNS>@\<KDC-REALM>)
2. If the attached user to the SPN exist and is unique.
3. Yhat the KVNO in the keytab is the same on the attached user.
4. On the account:
    1. Is active
    2. does not expire (it's a service account not a real user)
    3. Check that DES is disabled.
5. The adress type (it should not be a A type address)
6. The AES128/256 Kerberos encryption are active on this account and are in SPN ciphers list
7. kinit

The script output will be a Warn if :

1. There is multiple SPN for one unique account
2. Address isn't resolved by the DNS
3. The AES128/256 Kerberos encryption are disables on the account

## Examples

keytab OK :

```command

[hebus@awesome-vm ~]$ ./checkkeytab.sh keytabs/mykeytab-dev.keytab my_user

------------------

my_user in MY_DOMAIN.LOCAL password:

KVNO 15 OK

HTTP/keycloak-test.my_compagny.dns.local is type A. OK

------------------


SERVICE_ACCOUNT@MY_DOMAIN.LOCAL has AES128 enabled. OK

Keytab has AES128 cipher for SPN related to SERVICE_ACCOUNT@MY_DOMAIN.LOCAL OK

SERVICE_ACCOUNT@MY_DOMAIN.LOCAL has AES256 enabled. OK

Keytab has AES256 cipher for SPN related to SERVICE_ACCOUNT@MY_DOMAIN.LOCAL OK

SERVICE_ACCOUNT@MY_DOMAIN.LOCAL seems GOOD: it is not disabled, never expires and DES is disabled. OK

------------------

kinit successful. Everything is OK.
```

keytab KO:

```command
[hebus@awesome-vm ~]$ ./checkkeytab.sh keytabs/mykeytab-dev-kvomismatch.keytab my_user

------------------

my_user in MY_DOMAIN.LOCAL password:

KVNO MISMATCH!!! Please regenerate the keytab. KO

Keytab: 4 SERVICE_ACCOUNT: 5

Exiting...
```